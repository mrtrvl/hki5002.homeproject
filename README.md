# README #

See projekt on mõeldud Programmerimine 1 koduse projekti jaoks.
Projektiks on javascriptis tehtud html canvasel tile based tankimäng.

Peaks olema siis midagi sellist, kus väljakul sõidad tankiga ringi ja hävitad vaenlase sihtmärke, milleks võivad olla mingid suurtükitornid, tankid vms...

Mäng peaks tulema multiplayer node serveril kasutades socket.io-d

Kasutan mingil määral p5.js frameworki.

Koodi osas ammutasin abi ja ideid siit:
https://github.com/CodingTrain/Rainbow-ode/tree/master/CodingChallenges/CC_32.2_agario_sockets