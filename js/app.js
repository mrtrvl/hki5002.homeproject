let tank;
let walls = [];
let bullets = [];
let fieldRows = 15;
let fieldColumns = 20;
let wallPieceSize = 40;
let canvasSizeXAxis = fieldColumns * wallPieceSize;
let canvasSizeYAxis = fieldRows * wallPieceSize;
let destroyableCount = 10;
let shoot = 32;

function setup() { // Käivitatakse ainult ühe korra

    createCanvas(canvasSizeXAxis, canvasSizeYAxis);

    initWall();

    let tankStartPositionX = 100;
    let tankStartPositionY = 100;

    tank = new Tank(tankStartPositionX, tankStartPositionY);
    tank.debug = true;
}

function draw() {  // Käivitatakse iga framega
    background(51);
    drawField();
    manageTank();
    manageBullets();
}

function initWall() {
    for (let piecesInRow = 0; piecesInRow < fieldColumns; piecesInRow++) {
        walls.push(new PieceOfWall(piecesInRow * wallPieceSize, 0, wallPieceSize, false));
    }

    for (let destroyablePiece = 0; destroyablePiece < destroyableCount; destroyablePiece++) {
        let destroyablePieceX = int(random(fieldColumns)) * wallPieceSize;
        let destroyablePieceY = int(random(fieldRows)) * wallPieceSize;
        walls.push(new PieceOfWall(destroyablePieceX, destroyablePieceY, wallPieceSize, true));
    }
}

function drawField() {
    for (let pieceOfWall = 0; pieceOfWall < walls.length - 1; pieceOfWall++) {
        checkIfTankTouchingWall(walls[pieceOfWall]);
        checkIfBulletTouchingWall(walls[pieceOfWall]);

        if (walls[pieceOfWall].destroyed) {
            walls.splice(pieceOfWall, 1);
            break
        } else {
            walls[pieceOfWall].show();
        }
    }
}

function checkIfTankTouchingWall(pieceOfWall){
    if (pieceOfWall.checkContactBetweenPieceOfWallAndObject(tank.x, tank.y, tank.size)){
        if (pieceOfWall.destroyable) {
            pieceOfWall.destroyed = true;
        } else {
            // console.log('Tank seinas!');
        }
    }
}

function checkIfBulletTouchingWall(pieceOfWall){
    if (bullets.length > 0) {
        bullets.forEach(function (bullet) {
            if (pieceOfWall.checkContactBetweenPieceOfWallAndObject(bullet.x, bullet.y, bullet.size)){
                if (pieceOfWall.destroyable) {
                    pieceOfWall.destroyed = true;
                }
                bullet.onField = false;
            }
        })
    }
}


function manageTank() {
    tank.update();
    tank.show();
}

function initBullet() {
    bullets.push(new Bullet(tank.x, tank.y, tank.angle));
}

function manageBullets() {
    if (bullets.length > 0) {
        bullets.forEach(function (bullet) {
            bullet.update();
            bullet.show();
            if (!bullet.onField) {
                bullets.splice(bullet, 1);
            }
        })
    }
}

function keyPressed() {
    if (keyCode === RIGHT_ARROW) {
        tank.rotateTank(-1);
    }

    if (keyCode === LEFT_ARROW) {
        tank.rotateTank(1);
    }

    if (keyCode === UP_ARROW) {
        tank.action(1);
    }

    if (keyCode === DOWN_ARROW) {
        tank.action(-1);
    }

    if (keyCode === shoot) {
        initBullet();
    }
}