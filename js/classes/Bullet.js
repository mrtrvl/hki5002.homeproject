class Bullet {
    constructor(bulletStartLocationX, bulletStartlocationY, angle) {
        this.x = bulletStartLocationX;
        this.y = bulletStartlocationY;
        this.angle = angle;
        this.size = 5;
        this.speed = 5;
        this.onField = true;
    }

    update() { // Asukoha koordinaatide arvutamine
        this.x += Math.cos(radians(this.angle)) * this.speed;
        this.y += Math.sin(radians(this.angle)) * this.speed;
        if (this.outOfField()) {
            this.onField = false;
        }
    }

    outOfField() { // Kas on väljakult väljas
        return (this.x < this.radius || this.x > width - this.size || this.y < this.size || this.y > height - this.size);
    }

    show() { // Kuuli joonistamine
        rectMode(CENTER);
        fill(255, 0, 0);
        push();
        translate(this.x, this.y);
        rotate(radians(this.angle));
        rect(0, 0, this.size, this.size);
        pop();
    }
}