class PieceOfWall { // Seina tüki klass

    constructor(pieceLocationX, pieceLocationY, pieceSize, destroyable) {
        this.x = pieceLocationX + pieceSize / 2;    // Alguspunkt
        this.y = pieceLocationY + pieceSize / 2;
        this.size = pieceSize;                 // Tüki külje pikkus
        this.cornerRadius = 5;
        this.debug = false;
        this.destroyable = destroyable;
        this.destoyed = false;
    }

    show() { // Seinatüki joonistamine
        stroke(255);
        strokeWeight(1);
        rectMode(CENTER);
        this.colour = this.setColour();
        fill(this.colour);
        rect(this.x, this.y, this.size, this.size, this.cornerRadius);
        if (this.debug) {
            this.showLocationInfo();
        }
    }

    setColour() {
        let wallColour = 0;
        let destroyableColour = 100;
        if (this.destroyable) {
            return destroyableColour;
        } else {
            return wallColour;
        }
    }

    showLocationInfo() {
        fill(255);
        text(parseInt(this.x), this.x + 10, this.y + 20);
        text(parseInt(this.y), this.x + 10, this.y + 30);
    }

    checkContactBetweenPieceOfWallAndObject(objectX, objectY, objectSize){
        if (Math.abs(this.x - objectX) > 0 &&
            Math.abs(this.x - objectX) < this.size &&
            Math.abs(this.y - objectY) > 0 &&
            Math.abs(this.y - objectY) < this.size){
            if (this.destroyable){
                this.destroyed = true;
            }
            return true;
        } else {
            return false;
        }
    }
}
