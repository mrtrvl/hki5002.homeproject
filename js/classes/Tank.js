class Tank { // Tanki klass

    constructor(tankStartLocationX, tankStartLocationY) {
        this.x = tankStartLocationX;    // Alguspunkt
        this.y = tankStartLocationY;
        this.size = 40;                 // Tanki suurus
        this.speed = 0;                 // Kiirus
        this.turnRate = 15;             // Keeramise samm
        this.drivePower = 0.2;          // Liikumiskiiruse muutumise samm
        this.angle = 0;                 // Algnurk
        this.debug = false;             // Kas näidata tanki asukohainfot
    }

    action(direction) { // Edasi / tagasi liikumiskiirus
        this.speed += this.drivePower * direction;
    }

    rotateTank(direction) { // Tanki asendi nurk
        this.angle -= this.turnRate * direction;
    }

    update() { // Asukoha koordinaatide arvutamine
        let lastX = this.x;
        let lastY = this.y;
        this.x += Math.cos(radians(this.angle)) * this.speed;
        this.y += Math.sin(radians(this.angle)) * this.speed;
        if (this.outOfField()) {
            this.x = lastX;
            this.y = lastY;
            this.speed = 0;
        }
    }

    outOfField() { // Kas on väljakult väljas
        return (this.x < this.size / 2 || this.x > width - this.size / 2 || this.y < this.size / 2 || this.y > height - this.size / 2);
    }

    show() { // Tanki joonistamine
        rectMode(CENTER);
        fill(255);
        push();
        translate(this.x, this.y);
        rotate(radians(this.angle));
        rect(0, 0, this.size, this.size);
        if (this.debug) {
            this.showLocationInfo();
        }
        pop();
    }

    showLocationInfo() {
        fill(0, 102, 153);
        text(parseInt(this.x), -10, 0);
        text(parseInt(this.y), -10, 10);
    }
}